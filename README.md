# Oktavy 21-22

GYMVOD programovací seminář Oktavy 2021/2022

## Učitelé

**David Hájek**

- hajekdd@gmail.com
- FEL
- Java, php , python, javascript


### Soutěž Kasiopea
- https://kasiopea.matfyz.cz/

### TEST
- 24.11.2021 teorie + programování

### Online výuka - karanténa
- https://meet.google.com/yno-pivf-txw

### Pravidla

- Všichni se naučíme pracovat s GITem. Materiály budou na GitLabu.
- Discord - https://discord.gg/VCzz8rDTae
- Vždycky si to z něho na začátku hodiny stáhneme.
- Úkoly budou každý týden nebo ob týden. Na vypracování máte týden,
s dobrou výmluvou máte dva týdny. Jinak dostanete úkol navíc. Za
úkoly se budou dostávat jedničky. Neopisovat, poznám to. Odevzdávat
na svůj repositář.
- Budete mít projekt na semestr ve dvojicích. Témata si představíme
příští hodinu.
- Písemky budou asi dvě - část teoretická a část praktická.
- Při hodině budete spolupracovat.

## Maturita
- [x] Základy algoritmizace a programování
- [x] Datové typy a struktury programovacího jazyka
- [x] Cyklus a rekurze
- [x] Seznamy
- [X] Práce s textem a soubory
- [X] Algoritmy třídění a řazení
- [ ] Numerické algoritmy – aritmetika čísel
- [ ] Uživatelské rozhraní programu
- [ ] Objektově orientované programování
- [X] Grafika v jazyce C#


### Harmonogram

| Datum | Učitel | Téma | Přednášky | Pokročilé | Úkol |
| --- | --- | --- | --- | --- | --- |
| 8.9.2021 | David | Úvod; seznámení; stránka; úkoly; semestrálka; postup při řešení problému; vlastnosti algoritmu, základní struktury; basic úloha | [lec01](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.09.15/uvodni-prezentace.pdf) | | |
| 15.9.2021 | David | GIT, založit repo, nasdílet, ssh klíč; pull, add ., commit, push, rebase, merge | [lec02](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.09.15/prezentace02.pdf) | [adv01](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/advanced/2021.09.15) | |
| 22.9.2021 | David | GIT, programovací jazyky | [lec03](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.09.22/prezentace03.pdf) | [adv02](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/advanced/2021.09.22) | [hw01](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/homework/hw01) |
| 29.9.2021 | David | Procvičování | [lec04](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.09.29/prezentace04.pdf) |  |  |
| 6.10.2021 | David | datové typy hodnotové a referenční; předdefinované datové typy; převody mezi datovými typy a jejich využití; deklarace proměnných | [lec05](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.10.06/prezentace04.pdf) | [adv03](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/advanced/ad03) | [hw02](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/homework/hw02) |
| 13.10.2021 | David | základní struktury programovacího jazyka | [lec05.2](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.10.13/prezentace05.pdf) |  |  |
| 20.10.2021 | David | cykly a rekurze | [lec06](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.10.20/prezentace06.pdf) | [adv04](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/advanced/ad04) | [hw03](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/homework/hw03) |
| 27.10.2021 | | | | | |
| 03.11.2021 | David | Rekurze 2 a seznamy | [lec07](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.11.03/prezentace07.pdf) | [adv05](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/advanced/ad05) | [hw04](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/homework/hw04) |
| 10.11.2021 | David | Práce s textem + Práce se souborem | [lec08](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.11.10/prezentace08.pdf) [lec08.2](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.11.10/prezentace09.pdf) | [adv06](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/advanced/ad06)  | [hw05](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/homework/hw05)  |
| 17.11.2021 | David |  |  |  |  |
| 24.11.2021 | David | Test |  |  |  |
| 01.12.2021 | David | GUI | [lec11](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.12.01/prezentace11.pdf) | Kasiopea |  |
| 08.12.2021 | David | Paint a Timer | [lec12](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.12.08/prezentace12.pdf) | [adv07](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/advanced/ad07) |  |
| 15.12.2021 | David | OOP | [lec13](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.12.15/prezentace13b.pdf) |  |  |
| 22.12.2021 | David | snowstorm |  |  |  |
| 12.01.2022 | David | OOP 2 | [lec14](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2022.01.12/prezentace14.pdf) |  |  |
| 19.01.2022 | David | Eratosthen | [lec17](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2022.01.19/prezentace17.pdf) |  |  |
| 26.01.2022 | David | Roman | [lec18](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2022.01.26/prezentace18.pdf) |  |  |