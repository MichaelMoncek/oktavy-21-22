# Sněžení

Vaším úkolem je naprogramovat formulářovou aplikaci, kde implementujete animaci sněžení.

Součástí GUI bude možnost měnit intenzitu a rychlost sněžení, přepínat na déšť a měnit pozadí a simulovat tak noc/den.

Využijte OOP.


