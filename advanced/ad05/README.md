# Hanojské věže

## Úkol: přemístit disky na druhou jehlu s použitím třetí pomocné jehly, přičemž musíme dodržovat tato pravidla:

pomocné jehly, přičemž musíme dodržovat tato pravidla:

	- v každém kroku můžeme přemístit pouze jeden disk, a to vždy z jehly na jehlu (disky nelze odkládat mimo jehly),
	- není možné položit větší disk na menší.


![alt text](./img.png?raw=true)

## OUTPUT
Přesouvám disk z hromádky 1 na hromádku 3
Přesouvám disk z hromádky 1 na hromádku 2
Přesouvám disk z hromádky 3 na hromádku 2
Přesouvám disk z hromádky 1 na hromádku 3
Přesouvám disk z hromádky 2 na hromádku 1
Přesouvám disk z hromádky 2 na hromádku 3
Přesouvám disk z hromádky 1 na hromádku 3


## hint
static void Hanoj(ref int odkud, ref int pres, ref int kam, int kolik)

{

   if (kolik > 0)

   {

		//kod	

     	Console.WriteLine ("Přesouvám disk z hromádky " + odkud + " na hromádku " + kam);

     	//kod

   }  

}


static Main()

{

   	int odkudVzit = 1; int presCo = 2; int kamPresunout = 3;

   	int kolikDisku = 3; 

   	Hanoj(ref odkudVzit, ref presCo, ref kamPresunout, kolikDisku);

}
