# Polynomials

Úkolem je vytvořit konzolovou aplikaci, ve které jsou tři funkce pro základní operace s polynomy. Konkrétně se jedná o výpočet hodnoty polynomu, součet dvou polynomů a součin dvou polynomů. Položkami listů/polí budou čísla datového typu double odpovídající koeficientům daného polynomu. Definujme, že první položka listu/pole s indexem nula bude odpovídat koeficientu u konstantního členu, druhá koeficientu u lineárního členu, třetí koeficientu u kvadratického členu, atd…

Tudíž například list poly1 = [1, 2.5, 3.5, 0, 5.4] reprezentuje polynom:


![alt text](./img.png?raw=true)


## Výpočet hodnoty polynomu
Vytvořte funkci PolyEval s dvěma parametry (polynom, x), která bude vracet hodnotu polynomu v bodě x.

To znamená, že volání PolyEval(poly1, 0) vrátí 1, volání PolyEval(poly1, 2) vrátí 106.4, apod.

## Součet dvou polynomů
Vytvořte funkci PolySum s dvěma parametry (poly1, poly2), jež bude vracet nový polynom odpovídající součtu zadaných polynomů.

Například: volání PolySum([1, 2, 5], [2, 0, 1, -7]) vrátí [3, 2, 6, -7].

## Součin dvou polynomů
Vytvořte funkci PolyMultiply s dvěma parametry (poly1, poly2), jež bude vracet nový polynom odpovídající součinu zadaných polynomů.

Například: volání PolyMultiply([1, 2, 5], [2, 0, 1, -7]) vrátí [2, 4, 11, -5, -9, -35].
