﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace primes2
{
    interface IPrimeGenerator
    {
        List<int> PrimesFromIntervalOldWay();

        List<int> PrimesFromIntervalEratosthen();
    }
}
