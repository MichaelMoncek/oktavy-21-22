﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rekurze2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("rekurze pokracovani");
            //ulong n = ulong.Parse(Console.ReadLine());
            //ulong n = 1000000; //tohle uz rekurze nezvladne
            ulong n = 10;

            Console.WriteLine(Suma(n));
            Console.WriteLine(SumaR(n));
            Console.WriteLine();

            //x = Suma od 1 do n ( n^3)
            Console.WriteLine(SumaN3(n));
            //x = Suda suma od 1 do n ( n^3)
            Console.WriteLine(SumaN3Suda(n));

            Console.WriteLine();
            Console.WriteLine("Faktorial"); //maximalni cislo 20, pak dojde k preteceni longu
            Console.WriteLine(Faktorial(20));
            Console.WriteLine("eulerovo cislo");
            Console.WriteLine(Euler(20));


            Console.ReadKey();
        }

        //SUMY
        static ulong Suma(ulong n)
        {
            ulong result = 0;
            for (ulong i = 1; i <= n; i++)
            {
                result += i;
                if (i % 1000000000 == 0) Console.WriteLine(i);
            }

            return result;
        }

        static ulong SumaR(ulong n)
        {
            if (n == 0) return 0;

            return n + SumaR(n - 1);
        }

        // x = Suma od 1 do n ( n^3)
        static ulong SumaN3(ulong n)
        {
            if (n == 0)
                return 0;
            else
                return (ulong)Math.Pow(n, 3) + SumaN3(n - 1);
        }

        static ulong SumaN3Suda(ulong n)
        {
            if (n % 2 == 1)
                return 0;
            if (n == 0)
                return 0;
            else
                return (ulong)Math.Pow(n, 3) + SumaN3Suda(n - 2);
        }

        //SUM 1/n!
        static decimal Euler(int n)
        {
            if (n == 0) return 1;
            
            return (1M / Faktorial(n)) + Euler(n - 1);
        }

        // n! = n * (n - 1)!
        static long Faktorial(long n)
        {
            if (n == 1) return 1;
            
            checked
            {
                return n * Faktorial(n - 1);
            }
        }

    }
}
